const JsPlugins = require('./webpack/Js')
const PugPlugins = require('./webpack/Pug')
const CssPlugins = require('./webpack/Css')
const SassPlugins = require('./webpack/Sass')
const CopyPlugins = require('./webpack/Copy')
const HtmlPlugins = require('./webpack/Html')
const CleanPlugins = require('./webpack/Clean')

/* Minimize */
const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  output: {
    path: __dirname + '/dist',
    filename: './js/bundle.js'
  },
  entry: ['./src/js/app.js', './src/scss/style.scss'],
  module: {
    rules: [
      PugPlugins,
      JsPlugins,
      SassPlugins
    ]
  },
  plugins: [CleanPlugins(), CssPlugins(), CopyPlugins(), ...HtmlPlugins()],
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: true
      })
    ]
  }
}
