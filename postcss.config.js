const Cssnano = require('cssnano')
const Autoprefixer = require('autoprefixer')

module.exports = {
  plugins: [
    Autoprefixer(),
    Cssnano({
      preset: [
        'default', {
          discardComments: {
            removeAll: true
          }
        }
      ]
    })
  ]
}
