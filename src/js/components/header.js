import $ from 'jquery'

let lastScrollTop = 0

$(document).on('click', function (e) {
  const $target = $(e.target)
  
  if (!$target.closest('#header').length && $('#header').is(':visible')) {
    $('.header').removeClass('hide')
    $('.header-search').removeClass('show')
  }
})

$(window).on('scroll', function () {
  let st = window.pageYOffset || document.documentElement.scrollTop
  
  if (st > lastScrollTop) {
    $('header').addClass('active')
  }
  
  if (st <= 0) {
    $('header').removeClass('active')
  }
  
  lastScrollTop = st <= 0 ? 0 : st
})

$('.header-search').on('click', function (e) {
  e.stopPropagation()
})

$('.header__action_search').on('click', function () {
  $('.header').addClass('hide')
  $('.header-search').addClass('show')
})

$('.header__action_menu').on('click', function () {
  $(this).toggleClass('active')
  $('body').toggleClass('modal-open')
  $('.global-menu').toggleClass('show')
  
  $('.header__action_search, .header__action_account').toggleClass('hide')
})
