import $ from 'jquery'
import Quill from 'quill'

const quill = $('#editor').get(0) && new Quill('#editor', {
  theme: 'snow',
  placeholder: 'Описание проекта*'
})

$('[data-agreement]').on('change', function () {
  if ($(this)[0].checked) {
    $(this).parents('.form-container').find('.custom-btn').attr('disabled', false)
  } else {
    $(this).parents('.form-container').find('.custom-btn').attr('disabled', true)
  }
})

$('input[type=file]:not(.excluded)').on('change', function (e) {
  const input = e.target
  const $img = $(this).parent().find('img')
  
  if (input.files && input.files[0]) {
    const reader = new FileReader()
    
    reader.onload = function (e) {
      $img.attr('src', e.target.result)
    }
    
    reader.readAsDataURL(input.files[0])
  }
})

$('form[data-form-ajax]').on('submit', function (e) {
  const form = $(this)
  const url = form.attr('action')
  const method = form.attr('method')
  const editor = form.find('#editorArea')
  
  if (editor.length > 0) {
    editor.val(quill.getText())
  }
  
  const data = new FormData(this)
  data.delete('files')
  
  form.remove('.form-error')
  
  e.preventDefault()
  
  $.ajax({
    url,
    data,
    method,
    processData: false,
    contentType: false,
    success: function (res) {
      if (res.success) {
        form.parents('.modal').modal('hide')
        form.find('.form-input, .form-radio, .form-area, .form-checkbox').val('')
        
        setTimeout(function() {
          $('#success').modal('show')
        }, 500)
      }
  
      if (res.redirect) {
        window.location.href = res.redirect
      }
    },
    error: function (res) {
      $.each(res, function ($key, $errorBag) {
        $.each($errorBag, function ($key, $message) {
          form.append(`<small class="form-error" data-key="${$key}">${$message}</small>`)
        })
      })
    }
  })
})