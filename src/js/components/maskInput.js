import $ from 'jquery'
import Inputmask from 'inputmask'

$(document).ready(function() {
  const inputsArr = []
  const inputs = document.getElementsByTagName('input')
  
  for (let i = 0; i < inputs.length; i++) {
    if (inputs[i].getAttribute('type') === 'tel') {
      inputsArr.push(inputs[i])
    }
  }
  Inputmask({ mask: '+38 (099) 999-99-99' }).mask(inputsArr)
})
