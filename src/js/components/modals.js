import $ from 'jquery'

function initPortfolioSlider () {
  const $slider = $('#portfolioSlider')
  const $currentSlide = $('.modal-slider-pagination__count_current')
  const $totalSlide = $('.modal-slider-pagination__count_total')
  
  $slider.on('init', function (event, slick) {
    $totalSlide.text(slick.slideCount)
    $currentSlide.text(slick.currentSlide + 1)
  })
  
  $slider.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: `<div class="modal-slider-arrow modal-slider-arrow_prev"><img src="/images/SliderArrow.svg" alt="Prev"></div>`,
    nextArrow: `<div class="modal-slider-arrow modal-slider-arrow_next"><img src="/images/SliderArrow.svg" alt="Next"></div>`
  })
  
  $slider.on('afterChange', function (event, slick, currentSlide) {
    $currentSlide.text(currentSlide + 1)
  })
}

$(document).on('hidden.bs.modal', function () {
  if ($('.modal:visible').length) {
    $('body').addClass('modal-open')
  }
})

$('[data-target="#portfolioModal"]').on('click', function () {
  setTimeout(function () {
    initPortfolioSlider()
  }, 500)
})
