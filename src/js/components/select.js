import $ from 'jquery'

class Select {
  constructor(htmlSelect, htmlOptions) {
    this.$select = htmlSelect
    this.$options = htmlOptions
    this.$styledSelect = {}
    this.$selectList = {}
    this.selectListItems = {}
    this.chipsWrap = {}
    this.chipsList = {}
    this.chips = []
  }
  
  get styledSelect() {
    return this.$styledSelect
  }
  
  get listItem() {
    return this.selectListItems
  }
  
  get chipsItem() { return this.chipsList }
  
  setValue(option) {
    const $selectInput = this.$styledSelect.find('span')
  
    this.$styledSelect.removeClass('active')
    $selectInput.text(option.name)
    this.$select.val(option.value)
    this.$selectList.hide()
  }
  
  setMultipleValue() {
    const $selectInput = this.$styledSelect.find('span')
    
    if (this.chips.length === 0) {
      this.initPlaceholder()
    } else {
      $selectInput.text(this.chips.map(chip => chip.name).join(', '))
    }
    
    this.$select.val(this.chips.map(chip => chip.value))
  }
  
  initPlaceholder() {
    this.$styledSelect.addClass('not-selected')
    this.$styledSelect.html(`<span>${this.$options.eq(0).text()}</span>`)
  }
  
  initOptions() {
    this.$selectList = $('<ul/>', { class: 'select-options' }).insertAfter(this.$styledSelect)
    
    for (let i = 0; i < this.$options.length; i++) {
      const $option = this.$options.eq(i)
      
      $('<li/>', { rel: $option.val(), text: $option.text() }).appendTo(this.$selectList)
    }
    
    this.selectListItems = this.$selectList.children('li')
  }
  
  addChips() {
    this.chipsWrap.html('')
    
    this.chips.forEach(chip => {
      $(`<li class="filter-chip"><span>${chip.name}</span><img class="delete" src="/images/CloseBlack.svg" alt="CLose"></li>`).appendTo(this.chipsWrap)
    })
  
    this.chipsList = this.chipsWrap.children('li')
  }
  
  deleteChip(index) {
    this.chips.splice(index, 1)
    $(this.chipsList[index]).remove()
    
    this.setMultipleValue()
    this.initChips()
  }
  
  initChips() {
    const $chipsWrap = this.$select.parents('.form-group').find('.filters-chips')
    
    this.chipsWrap = $chipsWrap
    
    if (this.chips.length > 0) {
      $chipsWrap.removeClass('d-none')
    } else {
      $chipsWrap.addClass('d-none')
    }
  }
  
  handleHideOptions() {
    if (this.$select.attr('data-select-type') === 'chip-select' && this.chips.length > 0) {
      this.$styledSelect.addClass('active')
    } else {
      this.$styledSelect.removeClass('active')
    }
    this.$selectList.hide()
  }
  
  handleShowOptions(e) {
    const selectVal = this.$select.val()
    
    e.stopPropagation()
    if (this.$styledSelect.hasClass('active') && this.$select.attr('data-select-type') === 'chip-select') {
      this.$styledSelect.next('ul.select-options').toggle()
    } else {
      this.$styledSelect.toggleClass('active').next('ul.select-options').toggle()
    }
    
    $('ul.select-options li').removeClass('active')
    
    if (this.$select.attr('multiple')) {
      selectVal.forEach(value => {
        $(`ul.select-options li[rel=${value}]`).addClass('active')
      })
    } else {
      $(`ul.select-options li[rel=${selectVal}]`).addClass('active')
    }
  }
  
  handleSelectValue(e, item) {
    const $item = $(item)
    const option = { value: $item.attr('rel'), name: $item.text() }
    e.stopPropagation()

    $('div.select-styled.active').removeClass('not-selected')

    if (this.$select.attr('multiple')) {
      if ($item.hasClass('active')) {
        const chipValues = this.chips.map(chip => chip.value)
        const index = chipValues.indexOf(option.value)

        if (index !== -1) {
          this.chips.splice(index, 1)
        }
      } else {
        this.chips.push(option)
      }

      $item.toggleClass('active')
      this.setMultipleValue()
      
      if (this.$select.attr('data-select-type') === 'chip-select') {
        this.initChips()
        this.addChips()
      }
    } else {
      this.setValue(option)
    }
  }
  
  initSelect() {
    this.$select.addClass('select-hidden')
    this.$select.wrap('<div class="form-select"></div>')
    this.$select.after('<div class="select-styled"></div>')
    this.$styledSelect = this.$select.next('.select-styled')
    
    this.initPlaceholder()
    this.initOptions()
  }
}

$('select:not(.ql-header)').each(function(index, item) {
  const $select = $(item)
  const $options = $(item).children('option')
  const CustomSelect = new Select($select, $options)
  
  CustomSelect.initSelect()
  
  CustomSelect.styledSelect.on('click', function(e) {
    CustomSelect.handleShowOptions(e, this)
  })
  
  CustomSelect.listItem.on('click', function(e) {
    CustomSelect.handleSelectValue(e, this)
  
    if ($select.attr('data-select-type') === 'chip-select') {
      CustomSelect.chipsItem.find('.delete').on('click', function() {
        CustomSelect.deleteChip($(this).parent().index())
      })
    }
  })
  
  $(document).on('click', function() {
    CustomSelect.handleHideOptions()
  })
})
