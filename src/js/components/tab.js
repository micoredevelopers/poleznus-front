import $ from 'jquery'

$(document).ready(function() {
  let currentIndex
  
  const $navLink = $('.nav-link')
  const $tabWrap = $('.tab-content')
  const $indicator = $('.tab-indicator')
  
  $navLink.hover(function () {
    const $linkWrap = $(this).parent()
    const index = $linkWrap.index()
    
    $('.nav-link').removeClass('tab-active')
    $(this).addClass('tab-active')
    
    $indicator.css('transform', `translateX(${100 * (index - 1)}%)`)
  }, function () {
    const $linkWrap = $('.nav-link.active').parent()
    const index = $linkWrap.index()
    
    $('.nav-link').removeClass('tab-active')
    
    $indicator.css('transform', `translateX(${100 * (index - 1)}%)`)
  })
  
  $navLink.on('click', function () {
    const $linkWrap = $(this).parent()
    const index = $linkWrap.index()
    
    $indicator.css('transform', `translateX(${100 * (index - 1)}%)`)
    
    if ($linkWrap.index() < currentIndex) {
      $tabWrap.addClass('ltr')
      $tabWrap.removeClass('rtl')
    } else {
      $tabWrap.addClass('rtl')
      $tabWrap.removeClass('ltr')
    }
    
    currentIndex = index
  })
})
