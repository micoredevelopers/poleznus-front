import $ from 'jquery'
import 'bootstrap/dist/js/bootstrap.bundle.min'

import './functions'
import './components'

const id = $('main').attr('id')

switch (id) {
  case 'main-page':
    require('./pages/index')
    break
  case 'registration-page':
    require('./pages/registration')
    break
  case 'phone-recovery-page':
    require('./pages/phone')
    break
  case 'customer-info-page':
  case 'performer-info-page':
  case 'customer-profile-page':
  case 'performer-profile-page':
  case 'performer-password-page':
  case 'customer-portfolio-page':
  case 'performer-portfolio-page':
    require('./pages/performer')
    break
  case 'customer-start-page':
    require('./pages/customer')
    break
  case 'deals-page':
    require('./pages/deals')
    break
}
