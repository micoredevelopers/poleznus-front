import $ from 'jquery'
import 'slick-slider'

const ls = window.localStorage
const isThank = Number(ls.getItem('registration_thank'))

if (isThank !== 0) {
  $('.performer-recommend_thank').addClass('d-none')
  $('.performer-info-wrap').addClass('full-border')
}

$('.close-recommend').on('click', function () {
  $(this).parent().addClass('d-none')
  $('.performer-info-wrap').addClass('full-border')
  ls.setItem('registration_thank', '1')
})

$('#portfolioFile').on('change', function (e) {
  const input = e.target
  const $filesRow = $(this).parents('.form-group').find('.form-files-row')
  
  $filesRow.html('')

  if (input.files.length > 0) {
    for (let i = 0; i < input.files.length; i++) {
      const reader = new FileReader()

      reader.onload = function (e) {
        const url = e.target.result
  
        $filesRow.append(`
          <div class="form-img-wrap">
            <input type="file" class="d-none" name="file[${i}]" value="${input.files[i]}">
            <button type="button" class="btn-delete-img">
                <img src="/images/CloseSmall.svg" alt="Close img"/>
            </button>
            <img src="${url}" alt="Form img"/>
          </div>
        `)
      }

      reader.readAsDataURL(input.files[i])
    }
  }
  
  $filesRow.delegate('.form-img-wrap .btn-delete-img', 'click', function () {
    $(this).parent().remove()
  })
})

$('#performer-password-form, #customer-password-form').on('submit', function (e) {
  const url = $(this).attr('action')
  const data = new FormData(this)
  const inputs = $(this).find('input')
  e.preventDefault()
  
  if ($(inputs[1]).val() !== $(inputs[2]).val()) {
    $(this).append(`<small class="form-error">Пароли не совпадают</small>`)
  } else {
    $(this).find('small').remove()
    
    $.ajax({
      url,
      data,
      method: 'POST',
      processData: false,
      contentType: false,
      success: function (res, status) {
        console.log(res)
      },
      error: function (xhr, desc, err) {
        console.log({ xhr, desc, err })
      }
    })
  }
})

$('#portfolio-slider').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 1,
  prevArrow: '.slider-arrow_prev',
  nextArrow: '.slider-arrow_next',
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1
      }
    }
  ]
})
