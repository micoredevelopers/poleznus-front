import $ from 'jquery'

$('.form-icon-pass').on('click', function () {
  const $input = $(this).parent().find('input')
  
  $('.form-icon-pass').removeClass('hidden')
  $(this).addClass('hidden')
  
  if ($(this).hasClass('form-icon-pass-show')) {
    $input.attr('type', 'text')
  } else {
    $input.attr('type', 'password')
  }
})
