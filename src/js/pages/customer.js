import $ from 'jquery'

$('#orderFile').on('change', function (e) {
  const input = e.target
  const $filesRow = $(this).parents('.form-group').find('.form-files-row')
  
  $filesRow.html('')
  
  if (input.files.length > 0) {
    for (let i = 0; i < input.files.length; i++) {
      const reader = new FileReader()
      const file = input.files[i]
  
      reader.onload = function (e) {
        const url = e.target.result
  
        $filesRow.append(`
          <div class="form-img-wrap">
            <input type="file" class="d-none" name="file[${i}]" value="${input.files[i]}">
            <button type="button" class="btn-delete-img">
                <img src="/images/CloseSmall.svg" alt="Close img"/>
            </button>
            ${file.type.includes('image') ? `<img src="${url}" alt="Form img"/>` : `<p class="form-file-text">${file.name}</p>`}
          </div>
        `)
      }
      
      reader.readAsDataURL(input.files[i])
    }
  }
  
  $filesRow.delegate('.form-img-wrap .btn-delete-img', 'click', function () {
    $(this).parent().remove()
  })
})
