import $ from 'jquery'

function toggleModal(modalId) {
  const $modal = $(`#${modalId}`)
  
  if ($modal.hasClass('show')) {
    $modal.removeClass('show')
    $('html, body').removeClass('overflow-hidden')
  } else {
    $modal.addClass('show')
    $('html, body').addClass('overflow-hidden')
  }
}

function setPositionOfElements() {
  const offset = $('.filter-list').offset()
  const btnH = $('.filter-list').height()
  const modalBoyW = $('.filters-modal__body').width()
  
  $('.filters-modal__close').css({
    top: offset.top,
    left: offset.left - 72
  })
  
  $('.filters-modal__body').css({
    top: offset.top + btnH + 20,
    left: offset.left - modalBoyW + 30
  })
}

$(document).ready(function() {
  setPositionOfElements()
})

$(window).resize(function() {
  setPositionOfElements()
})

$('.filter-list, .filters-modal__close, .filters-modal__background').on('click', function() {
  toggleModal('filters')
})
