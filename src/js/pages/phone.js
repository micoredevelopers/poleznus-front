import $ from 'jquery'

const $codeInput = $('.code-input')

$codeInput.on('keyup', function () {
  const $input = $(this)
  const $submitBtn = $input.parents('.form-container').find('.custom-btn')
  
  if ($codeInput.length - 1 === $input.index()) {
    if ($input.val() !== '') {
      $submitBtn.attr('disabled', false)
    } else {
      $submitBtn.attr('disabled', true)
    }
  }
  
  setTimeout(function () {
    if ($input.val().length === parseInt($input.attr('maxlength'),10)) {
      $input.next('input').focus()
    } else {
      $input.prev('input').focus()
    }
  }, 0)
})

$codeInput.bind('paste', function (e) {
  const pastedData = e.originalEvent.clipboardData.getData('text').split('').map(el => Number(el))
  
  if (pastedData.length > 4 && pastedData.includes(NaN)) {
    e.preventDefault()
  } else {
    $codeInput.each((index, input) => {
      $(input).val(pastedData[index])
      $codeInput[$codeInput.length - 1].focus()
    })
  }
})
