module.exports = {
  rules: [
    {
      test: /\.pug$/,
      exclude: /node_modules/,
      use: ['pug-loader']
    }
  ]
}
