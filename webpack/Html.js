const glob = require('glob')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = () => {
  const pages = glob.sync(path.resolve(__dirname, '../src/pages/*.pug'))

  return pages.map(file => {
    const base = path.basename(file, '.pug')

    return new HtmlWebpackPlugin({
      hash: true,
      minify: true,
      filename: './' + base + '.html',
      template: './src/pages/' + base + '.pug'
    })
  })
}
