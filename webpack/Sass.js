const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  rules: [
    {
      test: /\.(sa|sc|c)ss$/,
      exclude: /node_modules/,
      include: path.resolve(__dirname, '../src/scss'),
      use: [
        'style-loader',
        MiniCssExtractPlugin.loader,
        {
          loader: 'css-loader',
          options: { sourceMap: true, url: false }
        },
        {
          loader: 'postcss-loader',
          options: {
            sourceMap: true,
            config: { path: './postcss.config.js' }
          }
        },
        {
          loader: 'sass-loader',
          options: { sourceMap: true }
        }
      ]
    }
  ]
}
